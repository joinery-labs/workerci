
FROM rust:1.57-alpine3.13
ENV WRANG_VER="v1.19.5/wrangler-v1.19.5-x86_64-unknown-linux-musl.tar.gz"


RUN apk update && apk --no-cache add curl \
    make gcc musl-dev linux-headers \
    build-base abuild binutils cmake extra-cmake-modules
RUN curl https://rustwasm.github.io/wasm-pack/installer/init.sh -sSf | sh
RUN cargo install -q worker-build

RUN curl -L "https://github.com/cloudflare/wrangler/releases/download/${WRANG_VER}" \
     | tar -C /tmp -xvzf-
RUN mv /tmp/dist/wrangler /usr/bin/


